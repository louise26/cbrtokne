@extends('layouts.cbr.home')
@section('content')


<div class="row">
    <div class="col-xl-6 col-lg-6 col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <div class="media">
                        <div class="media-body text-xs-left">
                            <h3 class="pink">Your Address</h3>

                           
                                @foreach($wallet as $wallet)

                            <img src="https://chart.googleapis.com/chart?chs=125x125&amp;cht=qr&amp;chl={{$wallet->address}}" width="70%">

                                 
                        </div>

                        <p>{{$wallet->address}}</p>

                      
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-6 col-lg-6 col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <div class="media">
                        <div class="media-body text-xs-left">
                            <h3 class="deep-orange">{{ $wallet->balance }}</h3>
                            <span>CBR Token Balance</span>
                        </div>
                        <div class="media-right media-middle">
                            @if($wallet->balance == 0)

                                    <a href="{{ URL::to('buytoken') }}" class="btn btn-info"> Buy CBR Token</a>

                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
          @endforeach
    
</div>

@endsection