<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\Http\Requests;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            return view('pages.admin.home');
        }
        $users = DB::table('ewallet')
                    ->select('*')
                     ->where('user_id', $user->id)
                     ->get();

                     $data = [] ;

            if(count($users) > 0){


                   $data = [
                                'wallet' => $users
                            ] ;


            }

            else {

                    $link  = file_get_contents("https://immtradersclub.com/member/create-wallet");
                    $ss = [] ;
                    $details = json_decode($link);
                    $wallet_address = "" ;
                    $wallet_key = "";
                    $phrase = "" ;

                    foreach($details as $key => $value) {
                           array_push($ss,$value);

                    }
                    $wallet_address = $ss[0];
                    $wallet_key     = $ss[1];
                    $phrase         = $ss[2];


                    DB::table('ewallet')->insert(
                            [
                                'user_id' => $user->id,
                                'address'    => $wallet_address,
                                'private_address' => $wallet_key,
                                'mnemonic'      => $phrase,
                                'balance'    => 0

                            ]
                        );

                    $wallet = DB::table('ewallet')
                                  ->select('*')
                                  ->where('user_id', $user->id)
                                  ->get();

                     $data = [

                                'wallet' => $wallet
                            ];



            }


        return view('layouts.cbr.dashboard',$data);
    }


    public function buytoken(Request $request) {
      
                    $user = Auth::user();
                    $ss = [] ;
                    $wallet = DB::table('ewallet')
                                  ->select('*')
                                  ->where('user_id', $user->id)
                                  ->get();

                     $btc = DB::table('btc_address')
                                  ->select('*')
                                  ->where('user_id', $user->id)
                                  ->where('is_used',0)
                                  ->get();

                      $address = "" ;

                      if(count($btc) > 0) {
                          foreach ($btc as $key => $value) {
                                $address = $value->btc_address;
                          }
                      }
                      else {

                               $my_xpub 		      = "xpub6DKZ7xbwVC1Lg4QM9EGVpCvx58o16VrWFuzhDstTAzbjY7hKTqVk2JheBBqPcsCR9JhJTJoqgcmrKPkgHBhK8P15KJKRBdmd18ZrG8HRwGR"; //company xpub | ambot sayo
                               //$my_xpub ="xpub6BnvEDqyN5A6PbxkaHPajN9X63Ac4HWfvi4u8yztZcchqQXLSJ7YdR7Bv7vmJgop4rhJeeTSS2nATVuftpPXYh2UxrSmBVWJMqdpfh3hAeu";
                               $my_api_key 	     = "3739630b-0c66-4dd9-b245-280779d9afa2";

                               $callback_url		 =  "https://cbrtoken.com/payment-notification/";

                               $resp 				     = file_get_contents("https://api.blockchain.info/v2/receive?key=" . $my_api_key . "&callback=" . urlencode($callback_url) . "&xpub=" .$my_xpub);
                               $response 			   = json_decode($resp);

                               foreach ($response as $key => $value) {
                                          array_push($ss,$value);
                               }

                               DB::table('btc_address')->insert(
                                                                 [
                                                                     'user_id' => $user->id,
                                                                     'btc_address'    => $ss[0],
                                                                 ]
                                                              );

                                   $btcs = DB::table('btc_address')
                                                    ->select('*')
                                                     ->where('user_id', $user->id)
                                                     ->where('is_used',0)
                                                     ->get();

                                    foreach ($btcs as $key => $value) {
                                                  $address = $value->btc_address;
                                    }

                   }

                     $data = [
                                'wallet' => $wallet,
                                'btc'    =>  $address
                            ];

             return view('layouts.cbr.buytoken',$data);

    }
    public function paybtc(Request $request) {

             $xpub 		    = "xpub6DKZ7xbwVC1Lg4QM9EGVpCvx58o16VrWFuzhDstTAzbjY7hKTqVk2JheBBqPcsCR9JhJTJoqgcmrKPkgHBhK8P15KJKRBdmd18ZrG8HRwGR";// company xpub | ambot sayo
             //$my_xpub ="xpub6BnvEDqyN5A6PbxkaHPajN9X63Ac4HWfvi4u8yztZcchqQXLSJ7YdR7Bv7vmJgop4rhJeeTSS2nATVuftpPXYh2UxrSmBVWJMqdpfh3hAeu";
             $api_key 	  = "3739630b-0c66-4dd9-b245-280779d9afa2";
             $amount 			= $request->get('amount');


    }


}
